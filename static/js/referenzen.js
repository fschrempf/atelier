$(function() {
	$(".contentBox ul li").hover(function() {
		var classNames = $(this).attr("class").toString().split(/\s+/);
		drawCities(classNames, false);
	});
	$(".contentBox ul li").mouseleave(function() {
		$(".reddots").css('opacity', '0');
	});
});