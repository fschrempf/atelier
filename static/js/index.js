// Redirect to mobile page for small screens
var windowWidth = window.screen.width < window.outerWidth ?
                  window.screen.width : window.outerWidth;

if (windowWidth < 768 && location.hash != "#no-mobile") {
	window.location.replace("index-mobile/");
}

$(function() {
	$("#topRow").fitText(1);
	$("#contentRows").fitText(1);

	$("#zeile1").fitText(1.8);
	$("#zeile2").fitText(1.8);
	$("#zeile3").fitText(1.8);

	var classNames = $("#map").attr("class").toString().split(/\s+/);
	drawCities(classNames, true);
});
