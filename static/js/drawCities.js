function drawCities(names, alwaysVisible) {
	var lat, lon, x, y, offsetX, offsetY, index=0;
	$.each(names, function (i, name) {
		switch(name) {
			case "esslingen":
				lat = 48.74;
				lon = 9.320;
				break;
			case "ulm":
				lat = 48.40;
				lon = 9.987;
				break;
			case "karlsruhe":
				lat = 49.0;
				lon = 8.4;
				break;
			case "reutlingen":
				lat = 48.50;
				lon = 9.204;
				break;
			case "berlin":
				lat = 52.52;
				lon = 13.40;
				break;
			case "ludwigsburg":
				lat = 48.89;
				lon = 9.195;
				break;
			case "luebeck":
				lat = 53.87;
				lon = 10.69;
				break;
			case "goeppingen":
				lat = 48.71;
				lon = 9.651;
				break;
			case "gmuend":
				lat = 48.80;
				lon = 9.805;
				break;
			case "wertheim":
				lat = 49.76;
				lon = 9.513;
				break;
			case "riedlingen":
				lat = 48.16;
				lon = 9.471;
				break;
			case "muenchen":
				lat = 48.14;
				lon = 11.58;
				break;
			case "stuttgart":
				lat = 48.78;
				lon = 9.183;
				break;
			case "koeln":
				lat = 50.94;
				lon = 6.960;
				break;
			case "marbach":
				lat = 48.94;
				lon = 9.265;
				break;
			case "beuren":
				lat = 48.57;
				lon = 9.401;
				break;
			case "ditzingen":
				lat = 48.82;
				lon = 9.067;
				break;
			case "kirchheim":
				lat = 48.65;
				lon = 9.452;
				break;
			case "landau":
				lat = 49.20;
				lon = 8.119;
				break;
			case "mergentheim":
				lat = 49.49;
				lon = 9.770;
				break;
			case "blaubeuren":
				lat = 48.41;
				lon = 9.784;
				break;
			case "frankfurt":
				lat = 50.11;
				lon = 8.682;
				break;
			case "bamberg":
				lat = 49.90;
				lon = 10.90;
				break;
			case "weilheim":
				lat = 47.84;
				lon = 11.14;
				break;
			case "bonn":
				lat = 50.73;
				lon = 7.098;
				break;
			case "mainz":
				lat = 49.99;
				lon = 8.247;
				break;
			default:
				if(!alwaysVisible)
					$(".reddots").eq(index).css('opacity', '0');
				return;
		}

		var lat_min = 47.27;
		var lon_min = 5.873;
		var lat_max = 54.98;
		var lon_max = 15.04;

		x = (lon - lon_min) / (lon_max - lon_min) * 100;
		y = (lat - lat_min) / (lat_max - lat_min) * 100;

		y -= 3;

		$(".reddots").eq(index).css({'bottom': y + "%", 'left': x + "%"});

		if(!alwaysVisible)
			$(".reddots").eq(index).css('opacity', '1');

		index++;
	});
}
