// Redirect to mobile page for small screens
var windowWidth = window.screen.width < window.outerWidth ?
                  window.screen.width : window.outerWidth;

if (windowWidth < 768 && location.hash != "#no-mobile") {
	window.location.replace("index-mobile/");
}

// Preload CSS background images
$(':hidden').each(function() {
  // Checks for background-image tab
  var backgroundImage = $(this).css("background-image");
  if (backgroundImage != 'none') {
	var imgUrl = backgroundImage.replace(/"/g,"").replace(/url\(|\)$/ig, "");
	$('<img/>')[0].src = imgUrl;
  }
});

$("#contentRows").fitText(10.5);

// Tab Nav
var realclick = true; // prevent adding history on hashchange

$(".contentSwitch").on('click', function() {
	id = $(this).attr('id');
	$(".contentSwitch").removeClass('selected');
	$( '#' + id).addClass('selected');
	$(".contentBox").addClass('hidden');
	$( '#content_' + id ).removeClass('hidden');
	$(".imageBox").addClass('hidden');
	$( '#img_' + id ).removeClass('hidden');
	if (realclick) {
		history.pushState(null, null,  '#'+id);
	}
}); 

$(window).on('hashchange', function(e){ // triggered by browser, previous and next button
	var id ='';
	realclick = false;
	if (location.hash!='') { // if has hash
		id = $(location.hash+".contentSwitch").attr('id');
		$('#'+id).click();
	}
	else { // no hash = initial page
		id = $(".first.contentSwitch").attr('id');
		$('#'+id).click();
	}
	realclick = true;
}).trigger('hashchange'); // bind event to the same selector as event-listener
